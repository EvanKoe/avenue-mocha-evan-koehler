import AsyncStorage from '@react-native-async-storage/async-storage';
import React, { useEffect, useState } from 'react';
import { StyleSheet, Text, View, TouchableOpacity, Image } from 'react-native'
import Button from '../components/Button';
import Input from '../components/Input';
import MyAccount from '../components/MyAccount';
import Title from '../components/Title';
import colors from '../globals/colors';
import { Storage } from '../globals/types';

const back = "https://img.icons8.com/ios-filled/50/89e2d0/back.png";

const LogIn = ({ navigation }: any) => {
  const [hasAnAccount, setIfHasAnAccount] = useState<boolean>(false);
  const [state, setState] = useState<number>(0);
  const [name, setName] = useState<string>('');
  const [mail, setMail] = useState<string>('');
  const [psw, setPsw] = useState<string>('');
  const [cPsw, setCPsw] = useState<string>('');
  const [store, setStore] = useState<Storage[] | null>(null);
  const [error, setError] = useState<string>('');

  const createAccount = async () => {
    let user: Storage = {
      mail: mail,
      pass: psw,
      name: name,
      picture: '',
      orders: []
    };

    if (!(/^\S+@\S+\.\S+$/.test(mail))) {
      return setError('Invalid email address');
    } else if (psw.length < 8) {
      return setError('Password length must be at least 8');
    } else if (psw !== cPsw) {
      return setError("Passwords don't match");
    }

    AsyncStorage.setItem('connect', JSON.stringify(
      store === null ? [user] : [...store, user]
    ));
    alert("Account created successfully. Please log in.");
    setState(2);
  }

  const logIn = async () => {
    const res = await AsyncStorage.getItem('connect');
    if (res === null) {
      setError("Please create an account before logging in.");
      return ;
    }
    const accounts: Storage[] = JSON.parse(res);
    accounts.map((e) => {
      if (e.mail === mail) {
        AsyncStorage.setItem('isConnected', JSON.stringify('true'));
        return setState(3);
      }
    });
    setError('Mail address or password incorrect');
  }

  useEffect(() => {
    AsyncStorage.getItem('connect').then((e) => {
      setStore(e === null ? e : JSON.parse(e));
    });
  }, []);

  useEffect(() => setError(''), [state]);

  return (
    <View style={styles.container}>
      <Title>My Account</Title>

      {/* Do you wanna login or sign up ? */}
      { state === 0 && (
        <View style={styles.centered}>
          <Button
            text='I want to create an account'
            onPress={() => setState(1)}
            secondary
          />
          <Button
            text='I already have one'
            onPress={() => setState(hasAnAccount ? 3 : 2)}
          />
        </View>
      ) }

      {/* Create an account */}
      { state === 1 && (
        <View style={{ flex: 1 }}>
          <View style={{ flexDirection: 'row', marginVertical: 10 }}>
            <TouchableOpacity onPress={() => setState(0)}>
              <Image source={{ uri: back }} style={styles.goBack} />
            </TouchableOpacity>
            <Title style={{ fontSize: 20 }} color={colors.ffg02}>Sign Up</Title>
          </View>
          <View style={{ marginTop: '10%' }}>
            <Text style={styles.error}>{ error }</Text>
            <Input onChange={setName} placeholder='Your name (can be empty)' />
            <Input onChange={setMail} placeholder='Your email address' />
            <Input onChange={setPsw} placeholder='Your password' hidden />
            <Input onChange={setCPsw} placeholder='Confirm your password' hidden />
            <View style={{ flexDirection: 'row', marginTop: 20 }}>
              <Button
                text='Cancel'
                secondary
                style={{ flex: 1 }}
                onPress={() => setState(0)}
              />
              <Button
                text='Submit'
                style={{ flex: 1, marginLeft: 10 }}
                onPress={createAccount}
              />
            </View>
          </View>
        </View>
      ) }

      {/* Log in */}
      { state === 2 && (
        <View>
          <View style={{ flexDirection: 'row', marginVertical: 10 }}>
            <TouchableOpacity onPress={() => setState(0)}>
              <Image source={{ uri: back }} style={styles.goBack} />
            </TouchableOpacity>
            <Title style={{ fontSize: 20 }} color={colors.ffg02}>Log In</Title>
          </View>

          <View style={{ marginTop: '20%' }}>
            <Text style={styles.error}>{ error }</Text>
            <Input onChange={setMail} placeholder='Your email address' />
            <Input onChange={setPsw} placeholder='Your password' hidden />
            <View style={{ flexDirection: 'row' }}>
              <Button
                text='Cancel'
                secondary
                style={{ flex: 1, marginRight: 10 }}
                onPress={() => setState(0)}
              />
              <Button
                text='Log in'
                style={{ flex: 1 }}
                onPress={logIn}
              />
            </View>
          </View>
        </View>
      ) }

      { state === 3 && <MyAccount mail={mail} setState={setState} /> }
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.bg0,
    flex: 1,
    paddingHorizontal: 20,
    paddingVertical: 10
  },
  centered: {
    marginTop: 'auto',
    marginBottom: 'auto'
  },
  signinButton: {
    paddingVertical: 15,
    paddingHorizontal: 20,
    borderRadius: 10,
    marginVertical: 5,
    borderColor: colors.bg2,
    borderWidth: 2
  },
  loginButton: {
    backgroundColor: colors.bg2,
    paddingVertical: 15,
    paddingHorizontal: 20,
    borderRadius: 10,
    marginVertical: 5
  },
  buttonText: {
    color: colors.ffg04,
    marginLeft: 'auto',
    marginRight: 'auto',
    fontSize: 20
  },
  goBack: {
    width: 20,
    height: 20,
    marginTop: 'auto',
    marginBottom: 'auto',
    marginRight: 20
  },
  error: {
    color: '#991111',
    fontSize: 18
  }
})

export default LogIn;