import React from 'react';
import { StyleSheet, View, Text } from 'react-native';
import Title from '../components/Title';
import colors from '../globals/colors';
import { setLimit } from '../globals/functions';
import { Order } from '../globals/types';

const SingleCommand = (props: any) => {
  const order: Order = props.route.params.order;
  const taxes: number = order.price / 100 * 20;
  const priceHT: number = order.price - taxes;
  const table = [
    { left: setLimit(order.title, 20), right: Number(priceHT).toFixed(2) + ' $' },
    { left: 'Taxes', right: Number(taxes).toFixed(2) + ' $' },
    { left: 'TOTAL', right: Number(order.price).toFixed(2) + ' $' }
  ];
  const date = new Date(order.date);

  return (
    <View style={styles.container}>
      <Title>Your receipt</Title>
      <Text style={[
        styles.mochaTitle
      ]}>Avenue Mocha - Coffee shop</Text>
      { table.map((e) => { return (
      <View style={styles.lineView} key={e.left + e.right}>
        <Text style={styles.p}>{ e.left }</Text>
        <View style={styles.line} />
        <Text style={styles.p}>{ e.right }</Text>
      </View>
      ) }) }

      <Text style={[
        styles.pdate,
        { marginTop: '20%' }
      ]}>
        Bought via the Mocha Avenue app on { date.toDateString() }
      </Text>
      <Text style={styles.pdate}>{ date.toTimeString() }</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.bg0,
    paddingTop: 20,
    paddingHorizontal: 20
  },
  lineView: {
    flexDirection: 'row',
    marginTop: '10%'
  },
  p: {
    color: colors.grey08,
    fontSize: 18
  },
  pdate: {
    fontSize: 18,
    color: colors.grey05,
    textAlign: 'center'
  },
  mochaTitle: {
    color: colors.ffg01,
    textAlign: 'center',
    marginVertical: '20%',
    fontSize: 20
  },
  line: {
    height: 2,
    borderRadius: 15,
    marginTop: 'auto',
    marginBottom: 'auto',
    marginHorizontal: 20,
    flex: 1,
    backgroundColor: colors.grey07
  }
});

export default SingleCommand;