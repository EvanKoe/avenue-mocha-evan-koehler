import React, { useEffect, useState } from "react";
import { StyleSheet, Image, View, Dimensions, Text, TouchableOpacity } from "react-native";
import colors from "../globals/colors";
import SlidingUpPanel from 'rn-sliding-up-panel';
import AsyncStorage from "@react-native-async-storage/async-storage";
import { Coffee, Order } from "../globals/types";
import Switcher from "../components/Switcher";
import Button from "../components/Button";

const HEIGHT = Dimensions.get('screen').height;
const addToCard = "https://img.icons8.com/material-outlined/96/1B2223/shopping-basket-add.png";
const back = "https://img.icons8.com/ios-filled/50/1b2223/back.png"

const SingleScreenCatalog = (props: any) => {
  let slideRef: SlidingUpPanel | null = null;
  const coffee: Coffee = props.route.params.coffee;
  const image: string = props.route.params.image;
  const price: number = props.route.params.price;
  const nav: any = props.route.params.nav;
  const [sugar, setSugar] = useState<number>(0);
  const [size, setSize] = useState<number>(0);
  const [ice, setIce] = useState<number>(0);
  const [cream, setCream] = useState<number>(0);
  const [isCo, setIfCo] = useState<boolean>(false);

  const getPrice = () => {
    return (parseFloat(price) + parseInt(size) + parseInt(cream)).toFixed(2);
  };

  const makeOrder = async () => {
    let res = await AsyncStorage.getItem('orders');
    let orders: Order[] = res !== null ? JSON.parse(res) : [];
    let obj: Order = {
      description: coffee.description,
      id: orders.length === 0 ? 0 : orders[orders.length - 1].id + 1,
      ingredients: coffee.ingredients,
      title: coffee.title,
      price: getPrice(),
      date: Date.now()
    };
    await AsyncStorage.setItem('orders', JSON.stringify([...orders, obj]));
    alert('Your order has been noted. See "My orders".');
  };

  useEffect(() => {
    if (!coffee || !image) {
      console.log('[-] Coffee or image is undefined !')
    }
    AsyncStorage.getItem('isConnected').then((res) => {
      if (res === null || JSON.parse(res) === 'false') {
        return alert("You are not connected. Try log in before")
      }
      return setIfCo(true);
    });
  }, []);

  return (
    <View style={styles.container}>
      <TouchableOpacity onPress={() => nav.goBack()} style={styles.goBack}>
        <Image source={{ uri: back }} style={{ height: 30, width: 30 }} />
      </TouchableOpacity>
      { image ? (
        <Image style={styles.image} source={{ uri: image }} />
      ): (
        <Text style={styles.notFound}>Couldn't load image.</Text>
      ) }
      <SlidingUpPanel
        ref={(e) => slideRef = e}
        draggableRange={{ top: HEIGHT, bottom: 500 }}
        containerStyle={styles.slider}
        height={HEIGHT}
      >
        <View style={{ flex: 1 }}>
          {/* Draggable bar */}
          <TouchableOpacity
            style={styles.sliderBar}
            onPress={() => slideRef?.show()}
          />

          {/* header */}
          <View style={styles.header}>
            <Text style={styles.title}>{ coffee?.title ?? 'Title' }</Text>
            <Text style={styles.price}>{ price ? price + ' $' : 'PRICE' }</Text>
          </View>

          {/* description */}
          <Text style={styles.description}>{ coffee.description }</Text>

          {/* Ingredients */}
          <Text style={[
            styles.description,
            { marginTop: 10 }
          ]}>Ingredients :</Text>
          { coffee.ingredients.map((e) => { return (
            <Text key={e} style={styles.description}>{ '- ' + e }</Text>
          )}) }

          {/* Sugar */}
          <Switcher
            number={sugar}
            setNumber={setSugar}
            minMax={[0, 5]}
            text='Sugar cubes'
            style={{ marginTop: '10%' }}
          />
          <Text style={styles.addonSwitcherText}>Free</Text>

          {/* Size */}
          <Switcher
            number={size}
            setNumber={setSize}
            minMax={[0, 2]}
            text='Cup size'
          />
          <Text style={styles.addonSwitcherText}>
            Base price + 1$ / size
          </Text>

          {/* Ice */}
          <Switcher
            number={ice}
            setNumber={setIce}
            minMax={[0, 1]}
            text='Ice cubes'
            withOut
          />
          <Text style={styles.addonSwitcherText}>
            Free
          </Text>

          {/* Cream */}
          <Switcher
            number={cream}
            setNumber={setCream}
            minMax={[0, 1]}
            text='Cream'
            withOut
          />
          <Text style={styles.addonSwitcherText}>
            Base price + 1$
          </Text>

          {/* Place order */}
          <View style={{ flexDirection: 'row', marginTop: 'auto', marginBottom: 20 }}>
            <Text style={styles.description}>TOTAL</Text>
            <View style={styles.line} />
            <Text style={styles.description}>{ getPrice() + ' $' }</Text>
          </View>
          <Button
            disabled={!isCo}
            text={!isCo ? 'PLEASE LOGIN' : 'PLACE ORDER'}
            textStyle={isCo ? styles.addToCardText : [styles.addToCardText, { color: colors.grey07 }]}
            style={isCo ? styles.addToCardView : styles.addToCardDisabled}
            onPress={isCo ? makeOrder : () => alert("Please log in before ordering")}
          >
            <Image source={{ uri: addToCard }} style={styles.addToCard} />
          </Button>
        </View>
      </SlidingUpPanel>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.bg0,
    flex: 1,
    width: '100%'
  },
  goBack: {
    backgroundColor: colors.ffg01,
    borderRadius: 50,
    padding: 5,
    paddingLeft: 2,
    position: 'absolute',
    top: 20,
    left: 15,
    zIndex: 3
  },
  header: {
    marginHorizontal: 20,
    marginTop: '10%',
    marginBottom: '5%'
  },
  slider: {
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    backgroundColor: colors.bg1,
    elevation: 20,
    zIndex: 4
  },
  sliderBar: {
    backgroundColor: colors.ffg00,
    height: 5,
    width: 100,
    borderRadius: 50,
    marginVertical: 10,
    marginLeft: 'auto',
    marginRight: 'auto'
  },
  sliderArrow: {
    marginVertical: 5,
    marginLeft: 'auto',
    marginRight: 'auto'
  },
  image: {
    backgroundColor: colors.bg0,
    width: Dimensions.get('screen').width,
    height: Dimensions.get('screen').width
  },
  notFound: {
    color: colors.ffg00,
    marginLeft: 'auto',
    marginRight: 'auto',
    marginTop: 100
  },
  title: {
    color: colors.ffg02,
    fontSize: 30,
    fontWeight: 'bold',
  },
  price: {
    color: colors.ffg04,
    fontSize: 20,
    marginTop: 'auto',
    marginLeft: 'auto'
  },
  description: {
    marginHorizontal: 20,
    color: colors.fg1,
    textAlign: 'justify'
  },
  addToCard: {
    width: 30,
    height: 30
  },
  addToCardView: {
    backgroundColor: colors.ffg01,
    flexDirection: 'row',
    borderRadius: 10,
    marginLeft: 'auto',
    marginRight: 'auto',
    paddingHorizontal: 20,
    marginBottom: '20%',
    paddingVertical: 10
  },
  addToCardDisabled: {
    backgroundColor: colors.grey03,
    flexDirection: 'row',
    borderRadius: 10,
    marginLeft: 'auto',
    marginRight: 'auto',
    paddingHorizontal: 20,
    marginTop: 'auto',
    marginBottom: '20%',
    paddingVertical: 10
  },
  addToCardText: {
    fontWeight: 'bold',
    marginTop: 'auto',
    marginBottom: 'auto',
    color: colors.bg0,
    marginRight: 10
  },
  addToCardRoundView: {
    backgroundColor: colors.ffg01,
    position: 'absolute',
    zIndex: 2,
    bottom: 50,
    right: 20
  },
  addonSwitcherText: {
    textAlign: 'justify',
    color: colors.grey05,
    marginLeft: 40
  },
  line: {
    height: 2,
    borderRadius: 15,
    marginTop: 'auto',
    marginBottom: 'auto',
    marginHorizontal: 20,
    flex: 1,
    backgroundColor: colors.grey07
  }
});

export default SingleScreenCatalog;