import AsyncStorage from '@react-native-async-storage/async-storage';
import React, { useEffect, useRef, useState } from 'react';
import { Image, View, StyleSheet, Text, ImageBackground, Dimensions, TouchableOpacity } from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';
import SlidingUpPanel from 'rn-sliding-up-panel';
import Title from '../components/Title';
import colors from '../globals/colors';

const coffeeImg = "https://rainbowplantlife.com/wp-content/uploads/2020/11/mochacoffee2.jpg";
const HEIGHT = Dimensions.get('screen').height;

const Landing = () => {
  let slideRef = null;
  const [imgs, setImgs] = useState<string[]>([]);

  useEffect(() => {
    if (imgs.length > 2) {
      setImgs([]);
    }
    for (let i = 0; i < 2; ++i) {
      fetch("https://coffee.alexflipnote.dev/random.json")
      .then(res => res.json())
      .then(data => setImgs(e => [...e, data.file]));
    }
    AsyncStorage.setItem('isConnected', JSON.stringify('false'));
  }, []);

  return (
    <SafeAreaView>
      <ImageBackground source={{ uri: coffeeImg }} style={styles.container} blurRadius={5}>
        <SlidingUpPanel
          ref={(e) => slideRef = e}
          draggableRange={{ top: HEIGHT, bottom: 200 }}
          height={HEIGHT}
          containerStyle={styles.tile}
        >
          <View style={styles.tile}>
            <TouchableOpacity onPress={() => slideRef.show()}>
              <View style={styles.scrollDiv} />
            </TouchableOpacity>
            <Title style={styles.title}>Av. Mocha</Title>
            <View style={styles.disk} />
            <View style={styles.ttile}>
              <Image source={{ uri: coffeeImg }} style={styles.img} />
              <Text style={styles.p}>Make coffee great again with us.</Text>
            </View>
            { imgs.map((e, i) => {
              return (
                <View style={styles.ttile} key={e}>
                  { i % 2 === 0 && (
                    <Text style={styles.p}>{ writings[i] }</Text>
                  )}
                  <Image
                    style={styles.img}
                    source={{ uri: e }}
                    key={e}
                  />
                  { i % 2 !== 0 && (
                    <Text style={styles.p}>{ writings[i] }</Text>
                  )}
                </View>
              );
            }) }
          </View>
        </SlidingUpPanel>
      </ImageBackground>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    height: '100%'
  },
  tile: {
    backgroundColor: colors.bg0,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    paddingVertical: 5,
    paddingHorizontal: 15
  },
  disk: {
    borderColor: colors.ffg02,
    position: 'absolute',
    borderWidth: 50,
    height: 500,
    width: 500,
    borderRadius: 500,
    left: -200,
    top: 200
  },
  ttile: {
    backgroundColor: colors.bg1,
    flexDirection: 'row',
    padding: 15,
    borderRadius: 15,
    marginVertical: 10
  },
  img: {
    width: 150,
    height: 150,
    borderRadius: 12
  },
  p: {
    color: colors.fg1,
    flex: 1,
    fontSize: 18,
    marginHorizontal: 12,
    textAlign: 'justify'
  },
  title: {
    marginVertical: 20,
    color: colors.ffg02
  },
  scrollDiv: {
    backgroundColor: colors.ffg00,
    width: 100,
    height: 5,
    borderRadius: 50,
    marginLeft: 'auto',
    marginRight: 'auto',
    marginBottom: 5
  }
});

const writings: string[] = [
  "We use the best coffee beans to offer you the best quality.",
  "The only thing that is important to us is YOU."
];

export default Landing;