import AsyncStorage from "@react-native-async-storage/async-storage";
import React, { useEffect, useState } from "react";
import { FlatList, StyleSheet, Image, Text, View, TouchableOpacity, RefreshControl, Alert } from "react-native";
import Title from "../components/Title";
import colors from "../globals/colors";
import { Order } from "../globals/types";

const bin = "https://img.icons8.com/ios-filled/50/ffffff/delete-forever.png"

const Commands = ({ navigation }: any) => {
  const [orders, setOrders] = useState<Order[]>([]);
  const [refreshing, setRefresh] = useState<boolean>(false);
  const [total, setTotal] = useState<number>(0);

  const refresh = () => {
    setRefresh(true);
    AsyncStorage.getItem('orders').then((e) => {
      if (e === null) {
        return ;
      }
      let obj: Order[] = JSON.parse(e);
      let t: number = 0;
      setOrders(obj);
      obj.map((e: Order) => t += parseFloat(e.price.toString()));
      setTotal(t);
      setRefresh(false);
    })
  };

  const clear = () => {
    AsyncStorage.removeItem('orders').then(() => {
      setOrders([]);
      refresh();
    });
  };

  const showConfirmDialog = () => {
    return Alert.alert(
      "Clear",
      "Are you sure you want to remove all your orders ?",
      [
        { text: "No" },
        { text: "Yes", onPress: () => clear() }
      ]
    );
  };

  useEffect(() => {
    AsyncStorage.getItem('isConnected').then((e) => {
      if (e === null || JSON.parse(e) === false) {
        return setOrders([]);
      } else {
        refresh();
      }
    })
  }, []);

  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <Title style={{ marginRight: 'auto' }}>My orders</Title>
        <TouchableOpacity onPress={showConfirmDialog}>
          <Image source={{ uri: bin }} style={styles.binIcon} />
        </TouchableOpacity>
      </View>
      { orders.length === 0 ? (
        <View style={{ marginTop: '20%' }}>
          <Text style={styles.notFound}>It's quite empty here !</Text>
          <Text style={styles.notFound}>Try refreshing if you don't see your orders !</Text>
          <Text style={styles.notFound}>Be sure to log in before ordering !</Text>
        </View>
      ) : (
        <View style={{ flexDirection: 'row' }}>
          <Text style={styles.notFound}>Total spent</Text>
          <Text style={styles.notFound}>{ Number(total).toFixed(2) + ' $' }</Text>
        </View>
      ) }

      <FlatList
        data={orders.reverse()}
        style={{ marginBottom: '15%' }}
        renderItem={(e) => { return e.item && (
          <TouchableOpacity
            key={e.item.id}
            style={styles.orderView}
            onPress={() => navigation.navigate('single_command', { order: e.item })}
          >
            <View>
              <Text style={styles.title}>{ e.item.title }</Text>
              <Text style={{ color: colors.grey05 }}>{ new Date(e.item.date).toDateString() }</Text>
            </View>
            <Text style={styles.price}>{ Number(e.item.price).toFixed(2) + ' $' }</Text>
          </TouchableOpacity>
        )}}
        refreshControl={
          <RefreshControl
            refreshing={refreshing}
            onRefresh={refresh}
            colors={[colors.ffg01, colors.fg1]}
            tintColor={colors.ffg01}
          />
        }
      />
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.bg0,
    height: '100%',
    paddingHorizontal: 20,
    paddingVertical: 10
  },
  header: {
    flexDirection: 'row',
    marginBottom: 40
  },
  notFound: {
    color: colors.ffg00,
    marginLeft: 'auto',
    marginRight: 'auto',
  },
  orderView: {
    backgroundColor: colors.bg1,
    paddingVertical: 15,
    paddingHorizontal: 30,
    borderRadius: 15,
    flexDirection: 'row',
    marginTop: 10
  },
  title: {
    color: colors.ffg01,
    fontWeight: 'bold',
    fontSize: 18,
    marginRight: 'auto'
  },
  price: {
    color: colors.ffg04,
    marginTop: 'auto',
    marginBottom: 'auto',
    marginLeft: 'auto'
  },
  binIcon: {
    width: 30,
    height: 30,
    padding: 15,
    marginTop: 'auto',
    marginBottom: 'auto'
  }
});

export default Commands;