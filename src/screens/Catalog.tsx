//* Standard import
import React, { useCallback, useEffect, useState } from 'react';
import {
  FlatList,
  StyleSheet,
  Switch,
  View,
  Text,
  TouchableOpacity,
  Image,
  RefreshControl
} from 'react-native';

//! Local import
import Title from '../components/Title';
import colors from '../globals/colors';
import { Coffee, ImageObject } from '../globals/types';
import { setLimit } from '../globals/functions';

//? Icons imports
import { FontAwesome5, FontAwesome } from '@expo/vector-icons';

const api = "https://api.sampleapis.com/coffee/";
const imgApi = "https://coffee.alexflipnote.dev/random.json";
const notFound = "https://img.icons8.com/ios-filled/500/c6f6ed/coffee-to-go.png"

const Catalog = ({ navigation }: any) => {
  const [coffees, setCoffees] = useState<Coffee[]>([]);
  const [images, setImages] = useState<ImageObject[]>([]);
  const [prices, setPrices] = useState<string[]>([]);
  const [isHot, setIfHot] = useState<boolean>(true);
  const [isLoaded, setLoaded] = useState<boolean>(false);
  const [refreshing, setRefreshing] = useState<boolean>(false);

  const onRefresh = () => {
    setRefreshing(true);
    setIfHot(true);
    setRefreshing(false);
  };

  const getCoffees = async () => {
    return fetch(api + (isHot ? 'hot' : 'iced'))
    .then((res) => res.json())
    .then((data) => data === null ? [] : data);
  };

  const getImage = async () => {
    let a: ImageObject = { file: notFound };
    const res = await fetch(imgApi);
    if (res === null) {
      return a;
    }
    return await res.json();
  };

  const reload = async () => {
    let imgs: ImageObject[] = [];
    let prcs: string[] = [];

    getCoffees().then(async (cof) => {
      setCoffees(cof);

      for (let i = 0; i < cof.length; ++i) {
        let a: string = Number(Math.random() * 5).toFixed(2);
        let img: ImageObject | null = await getImage();

        prcs.push(a);
        if (img === null) {
          return;
        }
        imgs.push(img);
      }
      setPrices(prcs);
      setImages(imgs);
    });
  };

  useEffect(() => {
    reload().then(() => {
      return setLoaded(true);
    });
  }, []);

  useEffect(() => { reload() }, [isHot]);

  return isLoaded ? (
    <View style={styles.container}>
      <View style={styles.subContainer}>
        <Title>Catalog</Title>
        <View style={styles.switchView}>
          <FontAwesome name="snowflake-o" size={24} color={colors.ffg03} style={styles.switchIcon} />
          <Switch
            trackColor={{ false: colors.ffg00, true: colors.fg0 }}
            thumbColor="#f4f3f4"
            ios_backgroundColor="#3e3e3e"
            onValueChange={setIfHot}
            value={isHot}
          />
          <FontAwesome5 name="mug-hot" size={24} color={colors.ffg03} style={styles.switchIcon} />
        </View>
      </View>

      <FlatList
        style={{ marginBottom: '16%' }}
        data={coffees}
        renderItem={({ item, index }) => {
          const isImg = images[index]?.file;

          return item.title !== 'hola' && item.title.length > 2 && (
            <TouchableOpacity
              style={styles.tile}
              onPress={() => navigation.navigate('single_catalog', {
                coffee: item, image: isImg ?? notFound, price: prices[index], nav: navigation
              })}
            >
              <View>
                <Text style={styles.tileTitle}>{ setLimit(item.title, 10) }</Text>
                <Text style={styles.p}>{ setLimit(item.description, 25) }</Text>
                <Text style={styles.price}>{ prices[index] ? prices[index]  + ' $' : 'Waiting ...' }</Text>
              </View>
              <Image source={{ uri: isImg ?? notFound }} style={styles.image} />
            </TouchableOpacity>
          );
        }}
        refreshControl={
          <RefreshControl
            refreshing={refreshing}
            onRefresh={onRefresh}
            colors={[colors.ffg01]}
          />
        }
      />
    </View>
  ) : (
    <View style={styles.loadingContainer}>
      <Text style={styles.loadingText}>Loading ...</Text>
    </View>
  )
};

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 10,
    backgroundColor: colors.bg0,
    paddingBottom: '25%'
  },
  subContainer: {
    flexDirection: 'row',
    padding: 10
  },
  switchView: {
    flexDirection: 'row',
    marginLeft: 'auto',
    padding: 5
  },
  switchIcon: {
    marginTop: 'auto',
    marginBottom: 'auto'
  },
  switchText: {
    color: colors.ffg03,
    marginTop: 'auto',
    marginBottom: 'auto',
    marginHorizontal: 5
  },
  tile: {
    backgroundColor: colors.bg1,
    padding: 15,
    borderRadius: 15,
    marginVertical: 5,
    flexDirection: 'row'
  },
  tileTitle: {
    color: colors.ffg02,
    fontWeight: 'bold',
    fontSize: 22,
    marginLeft: 10
  },
  p: {
    color: colors.fg1
  },
  image: {
    borderRadius: 15,
    width: 100,
    height: 100,
    marginLeft: 'auto',
    backgroundColor: colors.ffg00
  },
  price: {
    marginTop: 'auto',
    color: colors.ffg01
  },
  loadingContainer: {
    backgroundColor: colors.bg0,
    alignContent: 'center',
    height: '100%'
  },
  loadingText: {
    textAlign: 'center',
    color: colors.ffg00
  }
});

export default Catalog;