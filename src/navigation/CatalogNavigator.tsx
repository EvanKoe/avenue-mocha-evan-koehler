import React from "react";
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import Catalog from "../screens/Catalog";
import SingleScreenCatalog from "../screens/SingleScreenCatalog";

const CatalogNavigator = () => {
  const Stack = createStackNavigator();
  const noheader = { headerShown: false };

  return (
    <NavigationContainer independent>
      <Stack.Navigator>
        <Stack.Screen component={Catalog} name='catalog' options={noheader} />
        <Stack.Screen component={SingleScreenCatalog} name='single_catalog' options={noheader} />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default CatalogNavigator;