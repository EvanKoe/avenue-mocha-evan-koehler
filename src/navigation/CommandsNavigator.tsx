import React from "react";
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import Commands from "../screens/Commands";
import SingleCommand from "../screens/SingleCommand";

const CommandsNavigator = () => {
  const Stack = createStackNavigator();
  const noheader = { headerShown: false };

  return (
    <NavigationContainer independent>
      <Stack.Navigator>
        <Stack.Screen component={Commands} name='command' options={noheader} />
        <Stack.Screen component={SingleCommand} name='single_command' options={noheader} />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default CommandsNavigator;