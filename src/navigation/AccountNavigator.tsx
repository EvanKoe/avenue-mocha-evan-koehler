import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import React from 'react';
import { Text } from 'react-native';
import LogIn from '../screens/LogIn';

const AccountNavigator = () => {
  const Stack = createStackNavigator();
  const noheader = { headerShown: false };

  return (
    <NavigationContainer independent>
      <Stack.Navigator>
        <Stack.Screen component={LogIn} name='login' options={noheader} />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default AccountNavigator;