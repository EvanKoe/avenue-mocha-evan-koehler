import React, { FC, createContext, useState } from "react";
import { GlobalContextType } from "../globals/types";

export const GlobalContext = createContext<GlobalContextType | undefined>(undefined);

const GlobalContextProvider: FC = ({ children }) => {
  const [username, setUsername] = useState<string | undefined>(undefined);

  return (
    <GlobalContext.Provider value={{
      username,
      setUsername
    }}>
      { children }
    </GlobalContext.Provider>
  );
};

export default GlobalContextProvider;