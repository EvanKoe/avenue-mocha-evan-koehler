const colors = {
    bg0: '#1B2223',
    bg1: '#293435',
    bg2: '#3A4F50',
    fg0: '#0EF6CC',
    fg1: '#F4FEFD',

    // Fade based on FG0
    ffg00: '#4d7f75',
    ffg01: '#7bcbbb',
    ffg02: '#89e2d0',
    ffg03: '#96f6e4',
    ffg04: '#c6f6ed',

    // grey
    grey00: '#111',
    grey01: '#333',
    grey03: '#444',
    grey04: '#666',
    grey05: '#888',
    grey06: '#aaa',
    grey07: '#bbb',
    grey08: '#ddd',
    grey09: '#eee'
};

export default colors;