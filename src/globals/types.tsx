import { ImagePickerResult } from "expo-image-picker";
import { StyleProp, TextStyle, View } from "react-native";

type GlobalContextType = {
  username: string | undefined;
  setUsername: (e: string) => void;
};

type TitleProps = {
  children?: string;
  style?: TextStyle;
  color?: string;
}

type ImageObject = {
  file: string;
}

type Coffee = {
  description: string;
  id: number;
  ingredients: string[];
  title: string;
}

type SingleScreenCatalogType = {
  navigation: any;
  coffee: Coffee | undefined;
  image: string | undefined;
}

type SwitcherProps = {
  number?: number;
  setNumber?: (e: number) => void;
  minMax?: number[];
  text?: string;
  style?: any;
  withOut?: boolean;
};

type InputProps = {
  value?: string;
  placeholder?: string;
  placeholderColor?: string;
  style?: any;
  textStyle?: any;
  onChange?: (e: string) => void;
  centered?: boolean;
  hidden?: boolean;
};

type ButtonProps = {
  text?: string;
  secondary?: boolean;
  disabled?: boolean;
  onPress?: () => void;
  style?: any;
  textStyle?: any;
  children?: any;
};

type Order = {
  description: string;
  id: number;
  ingredients: string[];
  title: string;
  price: number;
  date: number;
};

type Storage = {
  mail: string;
  pass: string;
  name: string;
  picture: string;
  orders: Order[];
};

type MyAccountProps = {
  mail: string;
  setState: (e: number) => void;
}

type DispProps = {
  keyText?: string;
  valueText?: string;
  keyStyle?: any;
  valueStyle?: any;
  style?: any;
};

export {
  GlobalContextType,
  TitleProps,
  Coffee,
  ImageObject,
  SingleScreenCatalogType,
  Order,
  InputProps,
  SwitcherProps,
  ButtonProps,
  Storage,
  MyAccountProps,
  DispProps
};