const setLimit = (e: string, n: number) => {
  return (
    e.length > n ? e.substring(0, n) + '...' : e
  );
};

export {
  setLimit
}