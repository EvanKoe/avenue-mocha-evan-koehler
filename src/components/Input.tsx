import React from 'react';
import { TextInput, View, StyleSheet } from 'react-native';
import colors from '../globals/colors';
import { InputProps } from '../globals/types';

const Input = ({
  value = '',
  placeholder = "Input your text here",
  placeholderColor = colors.ffg00,
  style = {},
  textStyle = {},
  onChange = () => {},
  centered = false,
  hidden = false
}: InputProps) => {
  return (
    <View style={[
      styles.container,
      style
    ]}>
      <TextInput
        placeholder={placeholder}
        placeholderTextColor={placeholderColor}
        onChangeText={onChange}
        secureTextEntry={hidden}
        style={[
          styles.text,
          { marginLeft: centered ? 'auto' : 0 },
          { marginRight: centered ? 'auto' : 0 },
          textStyle
        ]}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.bg2,
    paddingVertical: 15,
    paddingHorizontal: 30,
    borderRadius: 15,
    marginVertical: 5
  },
  text: {
    fontSize: 18,
    color: colors.ffg01
  }
});

export default Input;