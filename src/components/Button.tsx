import React from 'react';
import { TouchableOpacity, Text, StyleSheet } from 'react-native';
import colors from '../globals/colors';
import { ButtonProps } from '../globals/types';

const Button = ({
  text = 'Click here',
  secondary = false,
  disabled = false,
  onPress = () => {},
  style = {},
  textStyle = {},
  children = <></>
}: ButtonProps) => {
  return (
    <TouchableOpacity
      style={[
        secondary ? styles.secondary : styles.primary,
        style
      ]}
      disabled={disabled}
      onPress={onPress}
    >
      <Text
        style={[
          styles.pText,
          secondary && { color: colors.ffg01 },
          textStyle
        ]}
      >{ text }</Text>
      { children }
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  primary: {
    backgroundColor: colors.ffg00,
    paddingVertical: 15,
    paddingHorizontal: 20,
    borderRadius: 15,
    marginVertical: 5,
    flexDirection: 'row'
  },
  secondary: {
    paddingVertical: 15,
    paddingHorizontal: 20,
    borderRadius: 15,
    marginVertical: 5,
    borderColor: colors.ffg00,
    borderWidth: 2,
    flexDirection: 'row'
  },
  pText: {
    color: colors.ffg04,
    marginLeft: 'auto',
    marginRight: 'auto',
    fontSize: 20
  }
});

export default Button;