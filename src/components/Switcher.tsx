import React from 'react';
import { Text, View, StyleSheet, TouchableOpacity } from 'react-native';
import colors from '../globals/colors';
import { SwitcherProps } from '../globals/types';

const Switcher = ({
  number = 0,
  setNumber = (e: number) => {},
  minMax = [0, 10],
  text = 'Value',
  style = {},
  withOut = false
}: SwitcherProps) => {
  return (
    <View style={[{ flexDirection: 'row' }, style]}>
      <Text style={styles.addonsText}>{ text }</Text>

      <TouchableOpacity
        onPress={() => setNumber(number - 1)}
        style={{ marginLeft: 'auto' }}
        disabled={number === minMax[0]}
      >
        <Text style={number === minMax[0] ? styles.disabledPlus : styles.plus}>-</Text>
      </TouchableOpacity>

      <Text style={styles.addonsText}>{ withOut ? (number === 0 ? 'Without' : 'With') : number }</Text>

      <TouchableOpacity
        onPress={() => setNumber(number + 1)}
        style={{ marginRight: 20 }}
        disabled={number === minMax[1]}
      >
        <Text style={number === minMax[1] ? styles.disabledPlus : styles.plus}>+</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  plus: {
    color: colors.ffg02,
    fontSize: 24,
    fontWeight: 'bold'
  },
  disabledPlus: {
    color: colors.grey04,
    fontSize: 24,
    fontWeight: 'bold'
  },
  addonsText: {
    color: colors.grey07,
    fontSize: 18,
    marginTop: 'auto',
    marginBottom: 'auto',
    marginHorizontal: 20
  }
});

export default Switcher;