import AsyncStorage from '@react-native-async-storage/async-storage';
import React, { useEffect, useState } from 'react';
import { View, StyleSheet, Image, TouchableOpacity } from 'react-native';
import { MyAccountProps } from '../globals/types';
import Button from './Button';
import Disp from './Disp';
import { Storage } from '../globals/types';
import { setLimit } from '../globals/functions';
import colors from '../globals/colors';
import * as ImagePicker from 'expo-image-picker';

const notFound = "https://img.icons8.com/ios-filled/100/4d7f75/user.png";
const emptyStorage = {mail: '', pass: '', name: '', picture: '', orders: []};
const hidImg = "https://img.icons8.com/ios-glyphs/30/4d7f75/hide.png";
const visImg = "https://img.icons8.com/ios-glyphs/30/4d7f75/visible--v1.png";

const MyAccount = ({
  mail = '',
  setState = () => {}
}: MyAccountProps) => {
  const [profile, setProfile] = useState<Storage>(emptyStorage);
  const [isHidden, setIfHidden] = useState<boolean>(true);

  const pickImage = async () => {
    const res = await ImagePicker.launchImageLibraryAsync();
    if (res.cancelled || res === null) {
      return;
    }
    const new_profile: Storage = { ...profile, picture: res.uri };
    const connect = await AsyncStorage.getItem('connect');
    if (connect === null) {
      return alert('Failed to save profile picture. Try again.');
    }
    const pres: Storage[] = JSON.parse(connect);
    await AsyncStorage.setItem('connect', JSON.stringify([
      ...(pres.filter((e) => e.mail !== mail)),
      new_profile
    ]))
    setProfile(new_profile);
    alert("Successfully set up your profile picture");
  }

  const logOut = () => {
    AsyncStorage.setItem('isConnected', JSON.stringify('false'));
    return setState(0);
  };

  useEffect(() => {
    AsyncStorage.getItem('connect').then((e) => {
      if (e === null) {
        return setState(0);
      }
      const accounts: Storage[] = JSON.parse(e);
      setProfile(emptyStorage);
      accounts.map((el) => {
        if (el.mail === mail) {
          return setProfile(el);
        }
      });
    })
  }, []);

  return (
    <View style={styles.container}>
      <View style={styles.main}>
        <TouchableOpacity onPress={pickImage}>
          <Image source={{ uri: profile?.picture ?? notFound }} style={styles.image} />
        </TouchableOpacity>
        <Disp
          keyText='Name'
          valueText={profile.name === '' ? 'Has not been defined' : setLimit(profile.name, 20)}
          style={styles.name}
        />
      </View>
      <Disp keyText='Email address' valueText={profile.mail} />
      <View style={styles.main}>
        <Disp
          keyText='Password'
          valueText={isHidden ? '********' : profile.pass}
        />
        <TouchableOpacity onPress={() => setIfHidden(e => !e)} style={styles.hiddenView}>
          <Image source={{ uri: isHidden ? visImg : hidImg }} style={styles.hiddenIcon} />
        </TouchableOpacity>
      </View>
      <Button onPress={logOut} text='Log out' style={styles.logOutButton} secondary />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  image: {
    width: 100,
    height: 100,
    borderRadius: 50,
    marginRight: 20,
    backgroundColor: colors.bg1,
    borderWidth: 2,
    borderColor: colors.ffg00
  },
  main: {
    flexDirection: 'row',
    marginVertical: 20
  },
  name: {
    marginTop: 'auto',
    marginBottom: 'auto'
  },
  logOutButton: {
    marginTop: 'auto',
    marginBottom: '20%'
  },
  hiddenIcon: {
    width: 25,
    height: 25
  },
  hiddenView: {
    marginLeft: 'auto',
    marginTop: 'auto',
    marginBottom: 'auto'
  }
});

export default MyAccount;