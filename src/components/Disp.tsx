import React from 'react';
import { Text, StyleSheet, View } from 'react-native';
import colors from '../globals/colors';
import { DispProps } from '../globals/types';

const Disp = ({
  keyText = 'Category',
  valueText = 'Value',
  keyStyle = {},
  valueStyle = {},
  style = {}
}: DispProps) => {
  return (
    <View style={style}>
      <Text style={[
        styles.cat,
        keyStyle
      ]}>{ keyText }</Text>
      <Text style={[
        styles.text,
        valueStyle
      ]}>{ valueText }</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  cat: {
    color: colors.ffg00
  },
  text: {
    color: colors.ffg02,
    fontSize: 18
  }
});

export default Disp;