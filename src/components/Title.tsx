import React from 'react';
import { Text, StyleSheet } from 'react-native';
import colors from '../globals/colors';
import { TitleProps } from '../globals/types';

const Title = ({
  children = 'Titre',
  style = {},
  color = '#fff'
}: TitleProps) => {
  return (
    <Text style={[
      styles.title,
      { color: color },
      style
    ]}>{ children }</Text>
  );
};

const styles = StyleSheet.create({
  title: {
    color: colors.fg1,
    fontSize: 40,
    fontWeight: 'bold'
  }
});

export default Title;