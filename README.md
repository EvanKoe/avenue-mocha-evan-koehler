# Avenue Mocha

Ceci est mon test d'entrée pour Taker Junior.

Le sujet est `Sujet_Taker_-_Avenue_Mocha.pdf`. N'hésitez pas à y jeter un oeil pour savoir ce qui était attendu.  

## Tester l'application

Pressé par le temps, je n'ai pu exporter l'application en .APK proprement. Je vous demande alors d'installer Expo Go sur votre smartphone (ou emulateur) et de le lancer depuis le serveur de developpement.

[App Store](https://apps.apple.com/us/app/expo-go/id982107779)
[Play Store](https://play.google.com/store/apps/details?id=host.exp.exponent&hl=en&gl=US)

Une fois l'application installée, clonez le repo :  
```bash
git clone https://gitlab.com/recrutement-dev-2022/avenue-mocha-evan-koehler.git
```
Si vous n'avez jamais lancé de projet React Native sur votre PC, installez le via votre package manager :  
```
npm nodejs yarn
```

Une fois dans le repo, installez les dépendances :  
```bash
yarn
```

Puis, lancez l'environnement de développement :  
```bash
yarn start
```

Un QR Code devrait apparaitre. Scannez-le avec l'application Expo Go si vous êtes sur Android, ou depuis votre caméra si vous êtes sur IOS.  

L'application se lance sur votre appareil. Vous pouvez maintenant la tester.  

## Merci