//* Standard import
import { StyleSheet } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { SafeAreaView } from 'react-native-safe-area-context';

//! Local imports
import AccountNavigator from './src/navigation/AccountNavigator';
import Landing from './src/screens/Landing';
import colors from './src/globals/colors';
import CommandsNavigator from './src/navigation/CommandsNavigator';

//? Icons
import { Entypo } from '@expo/vector-icons';
import GlobalContextProvider, { GlobalContext } from './src/context/global';
import CatalogNavigator from './src/navigation/CatalogNavigator';


const App = () => {
  const Tabs = createBottomTabNavigator();

  return (
    <GlobalContextProvider>
      <SafeAreaView style={styles.container}>
        <NavigationContainer independent>
          <Tabs.Navigator sceneContainerStyle={{ backgroundColor: colors.bg0 }}>
            <Tabs.Screen name="Landing" component={Landing} options={() => opts('home')} />
            <Tabs.Screen name="Catalogue" component={CatalogNavigator} options={() => opts('shop')} />
            <Tabs.Screen name="Commandes" component={CommandsNavigator} options={() => opts('shopping-basket')} />
            <Tabs.Screen name="Account" component={AccountNavigator} options={() => opts('user')} />
          </Tabs.Navigator>
        </NavigationContainer>
      </SafeAreaView>
    </GlobalContextProvider>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.bg0,
    height: '100%'
  },
  navbar: {
    backgroundColor: colors.bg2,
    borderTopWidth: 0,
    borderRadius: 15,
    marginHorizontal: 10,
    marginVertical: 12,
    height: 60,
    position: 'absolute',
    elevation: 20
  },
  navbarIcon: {
    padding: 0,
    margin: 0,
    marginTop: 'auto',
  }
});

const opts = (icon: string) => { return {
  headerShown: false,
  tabBarStyle: styles.navbar,
  tabBarLabel: '',
  tabBarIcon: ({ focused, color, size }) => (
    <Entypo name={icon} size={24} style={styles.navbarIcon} color={focused ? colors.ffg03 : colors.ffg00} />
  ),
}};

export default App;